#!/usr/bin/env fish

#the list of files is all the lines that aren't extensions with the pound sign indicating them
set files (cat $argv[1] | grep -vE "^#")
#temporary directory for the m3u file itself
set tempdir (mktemp --directory)
#the script changes directories to get the pathing relative to a given m3u file, so we need the realpath and not a relative one for the zip file
set zip_output (realpath $argv[2])
cd (dirname $argv[1])

for file in $files
	if string match --quiet --regex "^(http|https|rtmp)://" $file
		echo "we're not implementing anything to handle remote m3u listings. you're probably more weird and experienced than we with m3u if you see this message, friend"
		exit 1
	else
		# append the name of each file to the temp m3u file
		echo (basename $file) >> $tempdir/(basename $argv[1])
	end
end

# zip it up, ignore the directory structure, and add onto the very end, the zip file we just made
zip --junk-paths $zip_output $files $tempdir/(basename $argv[1])
